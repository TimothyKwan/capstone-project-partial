package com.example.jobtracker

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.jobtracker.ROOM.rJobPostingDatabase
import com.example.jobtracker.ROOM.rJobs

import com.google.android.material.tabs.TabLayout

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [OppotunityTabs.newInstance] factory method to
 * create an instance of this fragment.
 */
class OppotunityTabs : Fragment() {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var db: rJobPostingDatabase? = null

    private lateinit var tabLayout: TabLayout
    private lateinit var viewPager: ViewPager
    private lateinit var allJobListPage: OppotunityListPageFragment
    private lateinit var appliedListPage: OppotunityListPageFragment
    private lateinit var archiveListPage: OppotunityListPageFragment
    private lateinit var oppotunityPagerAdapter: OppotunityPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_oppotunity_tabs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        db = JobDatabaseManager.getInstance(activity?.applicationContext as Context)

        tabLayout = view.findViewById(R.id.opportunityTab)
        viewPager = view.findViewById(R.id.opportunityPager)

        tabLayout.tabGravity = TabLayout.GRAVITY_FILL
        allJobListPage = OppotunityListPageFragment(db?.jobPostingDao()?.getAllJobs() as List<rJobs>, "All Job")
        appliedListPage = OppotunityListPageFragment(db?.jobPostingDao()?.getJobsByStatus(JobStatus.APPLIED.ordinal) as List<rJobs>, "Applied Job")
        // TODO: Make two lists of jobs from COMPLETE_YES_OFFER and COMPLETE_NO_OFFER and pass that to fragment instead
        val archivedJobs = db?.jobPostingDao()?.getJobsByStatus(JobStatus.OFFER_ACCEPTED_ARCHIVE.ordinal)!! +
                db?.jobPostingDao()?.getJobsByStatus(JobStatus.NO_OFFER_ARCHIVE.ordinal)!!
        archiveListPage = OppotunityListPageFragment(archivedJobs, "Archive")

        tabLayout.setupWithViewPager(viewPager)


        oppotunityPagerAdapter = OppotunityPagerAdapter(childFragmentManager, FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT)
        oppotunityPagerAdapter.addFragment(allJobListPage, "All")
        oppotunityPagerAdapter.addFragment(appliedListPage,"Applied")
        oppotunityPagerAdapter.addFragment(archiveListPage,"Archive")
        viewPager.adapter = oppotunityPagerAdapter


        viewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int, positionOffset: Float, positionOffsetPixels: Int) {

            }

            override fun onPageSelected(position: Int) {
                //TODO: implement adapter update
                when (position){
                    0 ->{
                        Log.d("Page pressed ","Update page 1")
                        allJobListPage.updateList(db?.jobPostingDao()?.getAllJobs() as List<rJobs>)
                    }
                    1 ->{
                        Log.d("Page pressed ","Update page 2")
                        appliedListPage.updateList(db?.jobPostingDao()?.getJobsByStatus(JobStatus.APPLIED.ordinal) as List<rJobs>)
                    }
                    2 ->{
                        Log.d("Page pressed ","Update page 3")
                        val archivedJobs = db?.jobPostingDao()?.getJobsByStatus(JobStatus.OFFER_ACCEPTED_ARCHIVE.ordinal)!! +
                                db?.jobPostingDao()?.getJobsByStatus(JobStatus.NO_OFFER_ARCHIVE.ordinal)!!
                        archiveListPage.updateList(archivedJobs)
                    }
                }

            }

            override fun onPageScrollStateChanged(state: Int) {

            }

        })

        JobDatabaseManager.addListener(object: OpportunityListUpdateListener{
            override fun onUIUpdateCall() {
                updateItemList()
            }
        })

        JobDatabaseManager.setListPage(this)
    }

    fun updateItemList(){
        allJobListPage.updateList(db?.jobPostingDao()?.getAllJobs() as List<rJobs>)
        appliedListPage.updateList(db?.jobPostingDao()?.getJobsByStatus(JobStatus.APPLIED.ordinal) as List<rJobs>)
        val archivedJobs = db?.jobPostingDao()?.getJobsByStatus(JobStatus.OFFER_ACCEPTED_ARCHIVE.ordinal)!! +
                db?.jobPostingDao()?.getJobsByStatus(JobStatus.NO_OFFER_ARCHIVE.ordinal)!!
        archiveListPage.updateList(archivedJobs)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment OppotunityTabs.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() =
            OppotunityTabs().apply {
                arguments = Bundle().apply {

                }
            }


    }
}