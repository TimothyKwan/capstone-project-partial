package com.example.jobtracker

import android.content.Context
import android.util.Log
import androidx.room.Room
import com.example.jobtracker.ROOM.rJobPostingDatabase
import java.lang.ref.WeakReference

/**This class is a singleton that stores the database and allow the database object being called all over the app
 * date:19/11/2021      Timothy Kwan
 *
 * */
object JobDatabaseManager {
    private var INSTANCE: rJobPostingDatabase? = null
    private var listener = WeakReference<OpportunityListUpdateListener>(null)
    private lateinit var ListPage: OppotunityTabs

    fun getInstance(context: Context): rJobPostingDatabase?{
        if(INSTANCE == null){
            synchronized(this) {
                INSTANCE = Room.databaseBuilder(
                    context,
                    rJobPostingDatabase::class.java,
                    "JobPostingDatabase")
                    .allowMainThreadQueries()
                    .build()
            }
        }
        return INSTANCE
    }

    fun setListPage(ListPage:OppotunityTabs){
        this.ListPage = ListPage
    }

    fun updateUI(){
        Log.d("JobDatabaseManager","UpdateUI")
        //ListPage.updateItemList()
        listener.get()?.onUIUpdateCall()
    }

    fun addListener(newListener: OpportunityListUpdateListener){
        listener = WeakReference(newListener)
    }
}