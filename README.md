# Capstone Project Partial

This is from my capstone project "PaperTrail", a mobile application for people to keep track of their applied jobs. This repository contains part of the code about the app and mostly written by me.

## Contributing
Developers: Chris Smith, Jason Lee and Timothy Kwan
UX & UI designer: Rochelle Kriewaldt


## Project status
The project is currently closed