package com.example.jobtracker

import android.accounts.AccountManager
import android.content.Intent
import android.net.ConnectivityManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import com.example.jobtracker.googleUtil.CalendarServiceHolder
import com.example.jobtracker.googleUtil.CalendarServiceManager
import com.example.jobtracker.googleUtil.DriveServiceHelper
import com.example.jobtracker.googleUtil.DriveServiceManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.Scopes
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.api.client.json.JsonFactory
import com.google.api.client.extensions.android.http.AndroidHttp
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.gson.GsonFactory
import com.google.api.client.util.ExponentialBackOff
import com.google.api.services.calendar.Calendar
import com.google.api.services.calendar.CalendarScopes
import com.google.api.services.drive.Drive
import com.google.api.services.drive.DriveScopes
import kotlinx.coroutines.*
import java.util.*
import kotlin.math.log

//Source - https://github.com/miguelarauj1o/CalendarQuickStart
class MainActivity : AppCompatActivity(), View.OnClickListener {

    var credential: GoogleAccountCredential? = null
    private val transport: HttpTransport = AndroidHttp.newCompatibleTransport()
    private val jsonFactory: JsonFactory = GsonFactory.getDefaultInstance()
    private lateinit var signIn : com.google.android.gms.common.SignInButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        signIn = findViewById(R.id.sign_in_button)

        signIn.setOnClickListener(this)
        //GET PREFERRED ACCOUNT SETTINGS
        val settings = getPreferences(MODE_PRIVATE)
        //GET THE CREDENTIAL FOR GOOGLE API
        credential = GoogleAccountCredential.usingOAuth2(
            applicationContext, listOf(
                CalendarScopes.CALENDAR,
                DriveScopes.DRIVE_FILE,
                DriveScopes.DRIVE_READONLY
            )
        )
            .setBackOff(ExponentialBackOff())
            .setSelectedAccountName(
                settings.getString(
                    PREF_ACCOUNT_NAME,
                    null
                )
            )
        //INITIALIZE GOOGLE CALENDAR MANAGER
        //Log.d("Enum test int", JobStatus.getByValue(0).toString())
        //Log.d("Enum text string", JobStatus.SAVED.ordinal.toString())
        //Log.d("Current time in timestamp", FactoryMethods.getTimeStamp( FactoryMethods.getCurrentDateTime().toString()).toString())
        //Log.d("convert timestamp to dateTime", FactoryMethods.timeStampToDate(1637902800))

    }


    /**
    * Called whenever this activity is pushed to the foreground, such as after
    * a call to onCreate().
    */
    override fun onResume() {
        super.onResume()
        if (!isGooglePlayServicesAvailable) {
            Log.e("GooglePlayUnavailable", "Google Play Services required: after installing, close and relaunch this app.")
            return
        }
        if (credential!!.selectedAccountName != null){
            refreshResults()
        }
    }

    /**
    * Called when an activity launched here (specifically, AccountPicker
    * and authorization) exits, giving you the requestCode you started it with,
    * the resultCode it returned, and any additional data from it.
    * @param requestCode code indicating which activity result is incoming.
    * @param resultCode code indicating the result of the incoming
    * activity result.
    * @param data Intent (containing result data) returned by incoming
    * activity result.
    */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_GOOGLE_PLAY_SERVICES -> if (resultCode == RESULT_OK) {
                refreshResults()
            } else {
                isGooglePlayServicesAvailable
            }
            REQUEST_ACCOUNT_PICKER -> if (resultCode == RESULT_OK && data != null && data.extras != null) {
                val accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)
                if (accountName != null) {
                    credential!!.selectedAccountName = accountName
                    val settings = getPreferences(MODE_PRIVATE)
                    val editor = settings.edit()
                    editor.putString(PREF_ACCOUNT_NAME, accountName)
                    editor.apply()
                    refreshResults()
                }
            } else if (resultCode == RESULT_CANCELED) {
                Log.e("AccountError","Account unspecified.")
            }
            REQUEST_AUTHORIZATION -> if (resultCode == RESULT_OK) {
                refreshResults()
            } else {
                chooseAccount()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    /**
    * Attempt to get a set of data from the Google Calendar API to display. If the
    * email address isn't known yet, then call chooseAccount() method so the
    * user can pick an account.
    */
    private fun refreshResults() {
        if (credential!!.selectedAccountName == null) {
            chooseAccount()
        } else {
            if (!isDeviceOnline) {
                Log.e("NetworkError","No network connection available.")
            } else {
                setupGoogleDrive(credential as GoogleAccountCredential)
                setupGoogleCalendar(credential as GoogleAccountCredential)
                val intent = Intent(this, AppActivity::class.java)
                startActivity(intent)
                finish()
            }
        }
    }

    /**
    * Starts an activity in Google Play Services so the user can pick an
    * account.
    */
    private fun chooseAccount() {
        startActivityForResult(
            credential!!.newChooseAccountIntent(), REQUEST_ACCOUNT_PICKER
        )
    }

    /**
    * Checks whether the device currently has a network connection.
    * @return true if the device has a network connection, false otherwise.
    */
    private val isDeviceOnline: Boolean
    private get() {
        val connMgr = getSystemService(CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connMgr.activeNetworkInfo
        return networkInfo != null && networkInfo.isConnected
    }

    /**
    * Check that Google Play services APK is installed and up to date. Will
    * launch an error dialog for the user to update Google Play Services if
    * possible.
    * @return true if Google Play Services is available and up to
    * date on this device; false otherwise.
    */
    private val isGooglePlayServicesAvailable: Boolean
    private get() {
        val connectionStatusCode = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this)
        if (GoogleApiAvailability.getInstance().isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode)
            return false
        } else { return connectionStatusCode == ConnectionResult.SUCCESS }
    }

    /**
    * Display an error dialog showing that Google Play Services is missing
    * or out of date.
    * @param connectionStatusCode code describing the presence (or lack of)
    * Google Play Services on this device.
    */
    private fun showGooglePlayServicesAvailabilityErrorDialog(
        connectionStatusCode: Int
        ) {
        runOnUiThread {
            val dialog = GoogleApiAvailability.getInstance().getErrorDialog(
                this@MainActivity,
                connectionStatusCode,
                REQUEST_GOOGLE_PLAY_SERVICES
            )
            dialog.show()
        }
    }

    companion object {
        const val REQUEST_ACCOUNT_PICKER = 1000
        const val REQUEST_AUTHORIZATION = 1001
        const val REQUEST_GOOGLE_PLAY_SERVICES = 1002
        const val PREF_ACCOUNT_NAME = "accountName"
        val SCOPES = arrayOf(
            CalendarScopes.CALENDAR,
            Scopes.DRIVE_FILE,
            DriveScopes.DRIVE_READONLY
        )
    }

    //The method is used for setting up the GOOGLE DRIVE object
    private fun setupGoogleDrive(credential: GoogleAccountCredential){
        Log.d("setupGoogleDrive","initialize")
        var googleDriverService = Drive.Builder(
            AndroidHttp.newCompatibleTransport(),
            GsonFactory(),
            credential
        ).setApplicationName("GoogleDriveService")
            .build()
        var driveHelper = DriveServiceHelper(googleDriverService)
        DriveServiceManager.driveService = driveHelper
    }

    //The method is used for setting up the GOOGLE CALENDAR object
    private fun setupGoogleCalendar(credential: GoogleAccountCredential){
        var cService = Calendar.Builder(
            transport,
            jsonFactory,
            credential
        ).setApplicationName("GoogleCalendarService").build()
        var calendarManager = CalendarServiceManager(cService)
        CalendarServiceHolder.calendarService = calendarManager
    }

    override fun onClick(p0: View?) {
        refreshResults()
    }
}
