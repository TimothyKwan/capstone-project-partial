package com.example.jobtracker.ROOM

import androidx.room.*

/**
 * This class is for query in ROOM database. This controls the behaviour of the database.
 * date:19/11/2021      Timothy Kwan
 *
 * */
@Dao
interface rJobDAO {
    //the default get all will give all rJobs object as a List object
    @Query("SELECT * FROM Jobs ORDER BY LatestOpenDate DESC ")
    fun getAllJobs(): List<rJobs>

    //return the two jobs opened most recently
    @Query("SELECT * FROM jobs ORDER BY LatestOpenDate DESC LIMIT 2 ")
    fun getLatest2Jobs(): List<rJobs>

    //return all jobs based on the given job status. Note: it returns nothing if job status index is out of the list
    @Query("SELECT * FROM jobs WHERE JobTag = :status ORDER BY LatestOpenDate DESC ")
    fun getJobsByStatus(status:Int): List<rJobs>

    //return all CalendarEvents as a List object
    @Query("SELECT * FROM CalendarEvents")
    fun getAllCalendarEvents(): List<rCalendarEvents>

    //return a list of rCalendarEvents in ascending order based on the given timestamp
    @Query("SELECT * FROM CalendarEvents WHERE DateOfEvent >= :currentTimestamp ORDER BY DateOfEvent ASC")
    fun getLastestCalendarEvents(currentTimestamp: Long): List<rCalendarEvents>

    //return all Thumbnails as a List object
    @Query("SELECT * FROM ThumbNails")
    fun getAllThumbNails():List<rThumbNails>


    /**
     * Update, Insert, and Delete tags are short hand query that is build-in to ROOM
     * database.
     * */

    @Update
    fun updateJob(vararg newJob: rJobs)

    @Update
    fun updateCalendarEvents(vararg newCalendarEvents: rCalendarEvents)

    @Update
    fun updateThumbNails(vararg newThumbnails: rThumbNails)

    //This insert rJobs object and return rJobs id in database as a List of Long
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertJob(vararg newJob: rJobs):List<Long>

    //This insert rCalendarEvents and return rCalendarEvents id in database as a Long
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertCalendarEvents(vararg newCalendarEvents: rCalendarEvents):List<Long>

    //This insert rThumbNails and return rThumbNails id in database as a Long
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertThumbNails(vararg newThumbnails: rThumbNails):List<Long>

    @Delete
    fun deleteJob(vararg newJob: rJobs)

    @Delete
    fun deleteCalendarEvents(vararg newCalendarEvents: rCalendarEvents)

    @Delete
    fun deleteThumbNails(vararg newThumbnails: rThumbNails)

    //these are for getting a single object
    @Query("SELECT * FROM CalendarEvents WHERE EventID = :ID")
    fun getCalendarEvent(ID: Int): rCalendarEvents

    @Query("SELECT * FROM ThumbNails WHERE ThumbnailID = :ID")
    fun getTumbnail(ID:Int): rThumbNails

    //these are for getting list of objects based on the given jobID
    @Transaction
    @Query("SELECT * FROM Jobs WHERE JobID = :ID")
    fun getCalendarEventList(ID:Int): JobAndCalendar

    @Transaction
    @Query("SELECT * FROM Jobs WHERE JobID = :ID")
    fun getThumbnailList(ID:Int): JobAndThumbnail

    @Transaction
    @Query("SELECT * FROM Jobs WHERE JobID = :ID")
    fun getJobByID(ID:Int): rJobs

    @Transaction
    @Query("SELECT * FROM CalendarEvents WHERE DateOfEvent = (SELECT MAX(DateOfEvent) FROM CalendarEvents) AND cJobID = :ID ")
    fun getSoonestEvent(ID:Int): rCalendarEvents?
}