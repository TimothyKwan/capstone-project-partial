package com.example.jobtracker.ROOM

import androidx.room.Database
import androidx.room.RoomDatabase

/**This class is used for building the ROOM database
 * date:19/11/2021      Timothy Kwan
 *
 * */
@Database(entities = [rJobs::class, rCalendarEvents::class, rThumbNails::class], version =1)
abstract class rJobPostingDatabase: RoomDatabase() {
    abstract fun jobPostingDao(): rJobDAO
}