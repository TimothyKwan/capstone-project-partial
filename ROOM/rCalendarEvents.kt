package com.example.jobtracker.ROOM

import androidx.room.*

/**
 * This is a data class for ROOM database. The table name is CalendarEvents.
 * When the rJob object is delete, it will also delete the rCalendarEvents.
 * date:19/11/2021      Timothy Kwan
 * variable             notes
 * EventID              This stores the objectID.
 * CJobID               This is used to connect with the foreign key.
 * CalendarEventID      This store the calendar event ID as a String. This field can be null.
 * */
@Entity(tableName = "CalendarEvents", foreignKeys = [ForeignKey(entity = rJobs::class,
parentColumns = arrayOf("JobID"),
childColumns = arrayOf("CJobID"),
onDelete = ForeignKey.CASCADE)])
data class rCalendarEvents(
    @PrimaryKey (autoGenerate = true) val EventID:Int,
    val CJobID:Int,
    var CalendarEventID:String?,
    val DateOfEvent: Long?
)

//intermediate class for getting corresponding list of calendar event object from
//job posting object
data class JobAndCalendar(
    @Embedded val jobPosting: rJobs,
    @Relation(
        parentColumn = "JobID",
        entityColumn = "CJobID"
    )
    //this stores a list of Calendar events
    val eventList: List<rCalendarEvents>
)
