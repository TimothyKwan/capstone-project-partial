package com.example.jobtracker.ROOM

import androidx.room.*

/**
 * This is a data class for ROOM database. The table name is ThumbNails.
 * When the rJob object is delete, it will also delete the rThumbNails.
 * date:19/11/2021      Timothy Kwan
 * variable             notes
 * ThumbnailID          Store the thumbnail object ID
 * TJobID               Use as foreign key relation.
 * ThumbnailURI         Store the thumbnail URI as a string. This field can be null.
 * DocumentTag          Store the enum type as an Int to identify if it is resume or cover letter. This field can be null.
 * */
@Entity(tableName = "ThumbNails", foreignKeys = [ForeignKey(
    entity = rJobs::class,
    parentColumns = arrayOf("JobID"),
    childColumns = arrayOf("TJobID"),
    onDelete = ForeignKey.CASCADE
)])

data class rThumbNails(
    @PrimaryKey (autoGenerate = true) val ThumbnailID:Int,
    val TJobID:Int,
    var ThumbnailURI:String?,
    var DocumentTag:Int?
    )

data class JobAndThumbnail(
    @Embedded val jobPosting: rJobs,
    @Relation(
        parentColumn = "JobID",
        entityColumn = "TJobID"
    )
    val thumbnailList: List<rThumbNails>
)
